package com.example.shop.repository;

import com.example.shop.model.Customer;
import com.example.shop.model.Products;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
@Repository
public interface ProductRepository extends JpaRepository<Products, Long > {
    Optional<Products> findAllById(Long id);

}
