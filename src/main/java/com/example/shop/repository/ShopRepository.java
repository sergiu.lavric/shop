package com.example.shop.repository;

import com.example.shop.model.Shop;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ShopRepository  extends JpaRepository<Shop,Long> {
}
