package com.example.shop.repository;

import com.example.shop.model.Products;
import com.example.shop.model.ShoppingBag;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


import java.util.Optional;
@Repository
public interface ShoppingBagRepository  extends JpaRepository<ShoppingBag ,Long> {
    Optional<ShoppingBag> findAllById(Long id);
    Optional<ShoppingBag>findAllByCustomerId(Long  id);
}
