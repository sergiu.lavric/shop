package com.example.shop.controler;


import com.example.shop.dto.CustomerDto;

import com.example.shop.dto.ShopDto;
import com.example.shop.model.Customer;
import com.example.shop.service.ShopAndCustomerService;
import com.example.shop.util.HttpStatusHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/shop")
public class ShopAndCustomerControler {

    @Autowired
    ShopAndCustomerService shopAndCustomerService;

    public ShopAndCustomerControler(ShopAndCustomerService shopAndCustomerService) {
        this.shopAndCustomerService = shopAndCustomerService;
    }

    @PostMapping("/add-customer/{id}")

    public ResponseEntity addCustomer(@RequestBody CustomerDto customerDto, @PathVariable Long id) {
        try {
            return HttpStatusHelper.success("added customer ", shopAndCustomerService.addCustomer(customerDto, id));
        } catch (Exception e) {
            return HttpStatusHelper.commonErrorMethod(e);
        }
    }

//
    @PostMapping("/add-customer-to-shop")

    public ResponseEntity addCustomerToShop(@RequestBody CustomerDto customerDto, ShopDto shopDto) {
        try {
            return HttpStatusHelper.success("added customer to shop ", shopAndCustomerService.addCustomerToShop(customerDto, shopDto));
        } catch (Exception e) {
            return HttpStatusHelper.commonErrorMethod(e);
        }
    }

    @GetMapping("/get-customers/{id}")
    public Optional<Customer> getCustomers(@PathVariable Long id ) {
      return   shopAndCustomerService.getCustomer(id);
    }


    @PutMapping("/update-customer/{id}")
    public ResponseEntity updateEmployee (@RequestBody CustomerDto customerDto , @PathVariable Long id) {
        try {
            return HttpStatusHelper.success("Update customer",shopAndCustomerService.updateCustomer(customerDto,id));
        }catch (Exception e ) {
            return HttpStatusHelper.commonErrorMethod(e);
        }
    }
}

