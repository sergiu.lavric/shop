package com.example.shop.controler;


import com.example.shop.dto.BrandDto;
import com.example.shop.dto.CustomerDto;
import com.example.shop.exception.EntityNotFound;
import com.example.shop.model.Brand;
import com.example.shop.service.BrandService;
import com.example.shop.util.HttpStatusHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/shop")
public class BrandController {

    @Autowired
    BrandService brandService;

    public BrandController(BrandService brandService) {
        this.brandService = brandService;
    }


    @PostMapping("/add-brand/{id}")
    public ResponseEntity<Object> addBrand(@RequestBody BrandDto brandDto, @PathVariable Long id) {
        try {
            return HttpStatusHelper.success("add brand ", brandService.addBrand(brandDto, id));
        } catch (Exception e) {
            return HttpStatusHelper.commonErrorMethod(e);
        }
    }

    @PutMapping("update-brand/{id}")
    public ResponseEntity<Object> updateBrand(@RequestBody BrandDto brandDto, @PathVariable Long id) {
        try {
            return HttpStatusHelper.success("add brand ", brandService.updateBrand(brandDto, id));
        } catch (Exception e) {
            return HttpStatusHelper.commonErrorMethod(e);
        }
    }
//
//    @GetMapping ("/get-brand-to-customer/{id}")
//    public ResponseEntity<Object> addCustomerToBrand (  BrandDto brandDto,@PathVariable Long id ) {
//        try {
//            return HttpStatusHelper.success("add  customer to brand ", brandService.customerLikesBrand(brandDto ,id));
//        } catch (Exception e) {
//            return HttpStatusHelper.commonErrorMethod(e);
//        }
//    }

    @GetMapping("/get-all-brands")
    public ResponseEntity<Object> getALl(Long id) {
        try{
            return HttpStatusHelper.success("get all brands" ,brandService.getBrands(id));


        } catch (Exception e ) {
            return HttpStatusHelper.commonErrorMethod(e);
        }
    }


    @GetMapping("/get-brand/{id}")
    public ResponseEntity<Object> getBrands(@PathVariable Long id) {
        try {
            return HttpStatusHelper.success("add brand ", brandService.getBrands(id));
        } catch (Exception e) {
            return HttpStatusHelper.commonErrorMethod(e);
        }
    }
    @PostMapping ("/put-brand-by-customer/{id}")
    public ResponseEntity<Object> getBrandByCustomer (@PathVariable Long id,BrandDto brandDto) {
        try {
            return HttpStatusHelper.success("get brand",brandService.customerLikesBrand(brandDto, id));

        }catch (Exception e ) {
            return HttpStatusHelper.commonErrorMethod(e);
        }
    }


    @DeleteMapping("delete-brand-by-id/{id}")
    public String deleteByID(@PathVariable Long id) throws EntityNotFound {
        return brandService.deleteByID(id);
    }
}
