package com.example.shop.controler;


import com.example.shop.dto.EmployeeDto;
import com.example.shop.dto.ShopDto;
import com.example.shop.exception.EntityNotFound;
import com.example.shop.service.ShopAndEmployeService;
import com.example.shop.util.HttpStatusHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/shop")
public class ShopAndEmployeeControler {

    @Autowired
    ShopAndEmployeService shopAndEmployeService;



    @PostMapping("/add-revenue-and-employee")
    public ResponseEntity addRevenueAndEmployee(@RequestBody ShopDto shopDto, Long id) {
        try {
            return HttpStatusHelper.success("added revenue and employe list ", shopAndEmployeService.addRevenueAndEmployList(shopDto,id));
        }catch (Exception e ) {
            return HttpStatusHelper.commonErrorMethod(e);
        }
    }

    @PostMapping("/add-employee/{id}")
    public ResponseEntity addEmployee(@RequestBody  EmployeeDto employeeDto,@PathVariable Long id) {
        try {
            return HttpStatusHelper.success("added-employee ",shopAndEmployeService.addEmployee(employeeDto,id));

        }catch (Exception e ) {
            return HttpStatusHelper.commonErrorMethod(e);
        }

    }
    @PostMapping("/update-employee/{id}")
    public ResponseEntity updateEmployee (@RequestBody EmployeeDto employeeDto , @PathVariable Long id) {
        try {
            return HttpStatusHelper.success("Update employe",shopAndEmployeService.updateEmployee(employeeDto,id));
        }catch (Exception e ) {
            return HttpStatusHelper.commonErrorMethod(e);
        }
    }

    @PostMapping("/put-shop/{id}")
    public ResponseEntity putShop (@RequestBody ShopDto shopDto, @PathVariable Long id) {
        try {
            return HttpStatusHelper.success("Update Shop",shopAndEmployeService.putShop(shopDto,id));

        }catch (Exception e){
            return HttpStatusHelper.commonErrorMethod(e);
        }
    }


}

