package com.example.shop.controler;


import com.example.shop.dto.ProductDto;
import com.example.shop.dto.ShoppingBagDto;
import com.example.shop.exception.EntityNotFound;
import com.example.shop.model.ShoppingBag;
import com.example.shop.service.ShoppingBagService;
import com.example.shop.util.HttpStatusHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/shop")
public class ShoppingBagAndProductsController {

    @Autowired
    ShoppingBagService shoppingBagService;

    @PostMapping("/add-shopping-bag/{id}")
    public ResponseEntity addShoppingBag ( @RequestBody ShoppingBagDto shoppingBagDto,@PathVariable Long id  ) {
        try {
            return HttpStatusHelper.success("Added shoppingBag",shoppingBagService.shoppingBag(shoppingBagDto,id));
        } catch (Exception e ) {
            return   HttpStatusHelper.commonErrorMethod(e);
        }

    }

    @PostMapping("/add-products/{id}")
    public ResponseEntity addProducts(@RequestBody ProductDto productDto, @PathVariable Long id) throws EntityNotFound {
        try {
            return HttpStatusHelper.success("Added products", shoppingBagService.addProducts(productDto, id));
        } catch (Exception e) {
            return HttpStatusHelper.commonErrorMethod(e);
        }

    }

    @GetMapping("/get-products/{id}")
    public ResponseEntity getProducts (  @PathVariable Long id) throws EntityNotFound {
        try {
            return HttpStatusHelper.success("Added products", shoppingBagService.getProducts(id));
        } catch (Exception e) {
            return HttpStatusHelper.commonErrorMethod(e);
        }

    }

    @DeleteMapping("/delete-products-id/{id}")
    public String deleteProductsByID (@PathVariable Long id ) throws EntityNotFound {
        return shoppingBagService.deleteProductsByID(id);
    }


    @PostMapping("/add-to-shopping-bag")
    public ResponseEntity addToShopping(@RequestBody ProductDto productDto, ShoppingBagDto shoppingBagDto, Long id) throws EntityNotFound {
        try {
            return HttpStatusHelper.success("Added products", shoppingBagService.addToShoppingBag(shoppingBagDto, id, productDto));

        } catch (Exception e) {
            return HttpStatusHelper.commonErrorMethod(e);
        }
    }
    @GetMapping("/get-all/{id}")
    public ResponseEntity getAll( @PathVariable Long id) throws EntityNotFound{
       try{
           return HttpStatusHelper.success("get all shopping bag",shoppingBagService.findAll(id));
       } catch (Exception e) {
        return    HttpStatusHelper.commonErrorMethod(e);
       }

    }
    @GetMapping("/get-shopping-bag-by-id/{id}")
    public ResponseEntity getShoppingBagById( @PathVariable Long id) throws EntityNotFound {
        try {
            return HttpStatusHelper.success("get all shopping bag", shoppingBagService.getAllShoppingBag(id));
        } catch (Exception e) {
            return HttpStatusHelper.commonErrorMethod(e);
        }
    }

    @DeleteMapping("/delete-ShoppingBag-by-id/{id}")
    public String deleteByID (@PathVariable Long id ) throws EntityNotFound {
        return shoppingBagService.deleteShoppingBagByID(id);
    }

} 
