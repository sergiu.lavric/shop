package com.example.shop.service;


import com.example.shop.dto.ProductDto;
import com.example.shop.dto.ShoppingBagDto;
import com.example.shop.exception.EntityNotFound;
import com.example.shop.model.Customer;
import com.example.shop.model.Products;
import com.example.shop.model.Shop;
import com.example.shop.model.ShoppingBag;
import com.example.shop.repository.CustomerRepository;
import com.example.shop.repository.ProductRepository;
import com.example.shop.repository.ShoppingBagRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;

@Service
public class ShoppingBagService {

    @Autowired
    private  ShoppingBagRepository shoppingBagRepository;
    private  ProductRepository productRepository;
    private CustomerRepository customerRepository;



    public ShoppingBagService(ShoppingBagRepository shoppingBagRepository, ProductRepository productRepository) {
        this.shoppingBagRepository = shoppingBagRepository;
        this.productRepository = productRepository;
    }

    public List<ShoppingBag> findAll(Long id) {

        if (productRepository.existsById(id)) {
            return shoppingBagRepository.findAll();

        }throw new EntityNotFoundException("not found");
    }

    public String shoppingBag(ShoppingBagDto shoppingBagDto,Long id) {
        Customer customer = customerRepository.findById(id).orElseThrow(()-> new EntityNotFoundException("not Found"));
        ShoppingBag newShoppingBAg = new ShoppingBag(shoppingBagDto.getName(),shoppingBagDto.getCustomer());
        newShoppingBAg.setName(shoppingBagDto.getName());
        newShoppingBAg.setCustomer(customer);
       shoppingBagRepository.save(newShoppingBAg);
        return "added shopping-bag";
    }


    public Products addProducts(ProductDto productsDto, Long id) {
        ShoppingBag shoppingBag = shoppingBagRepository.findAllById(id).orElseThrow(()->new EntityNotFoundException("not found"));
        Products products = new Products();
        products.setAmountInBag(productsDto.getAmountInBag());
        products.setTypeOfProduct(productsDto.getTypeOfProduct());
        products.setShoppingBag(shoppingBag);
        productRepository.save(products);
        return products;

    }


    public Optional<ShoppingBag> addToShoppingBag(ShoppingBagDto shoppingBagDto, Long id, ProductDto productDto) {
        Optional<ShoppingBag> shoppingBag = shoppingBagRepository.findAllById(id);
        if (shoppingBag.isPresent()) {
            return shoppingBag.map(newShoppinglist -> {
                shoppingBagDto.setProductsList(productDto.getAmountInBag(), productDto.getTypeOfProduct(), productDto.getShoppingBag());
                shoppingBagRepository.save(newShoppinglist);
                return newShoppinglist;
            });
        }
        return shoppingBag;
    }

    public ShoppingBag getAllShoppingBag(Long id) throws EntityNotFound{
        Optional<ShoppingBag> optionalShoppingBag = shoppingBagRepository.findAllByCustomerId(id);
        if(optionalShoppingBag.isPresent()) {
            return optionalShoppingBag.get();
        }throw new EntityNotFound("I didn't find any shopping bag");
    }

    public String deleteShoppingBagByID(Long id)  {
       shoppingBagRepository.deleteById(id);
        return "deleted shoppind id ";
    }

    public Products getProducts(Long id) throws EntityNotFound {
        Optional<Products> optionalProducts = productRepository.findAllById(id);
        if(optionalProducts.isPresent()){
            return optionalProducts.get();
        }throw new EntityNotFound("I didn't find any products listed");

    }
    public String deleteProductsByID(Long id)  {
        productRepository.deleteById(id);
        return "deleted shoppind id ";
    }

}
