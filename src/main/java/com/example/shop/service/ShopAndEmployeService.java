package com.example.shop.service;


import com.example.shop.dto.EmployeeDto;
import com.example.shop.dto.ShopDto;
import com.example.shop.exception.EntityNotFound;
import com.example.shop.model.Employee;
import com.example.shop.model.Shop;
import com.example.shop.repository.EmployeeRepository;
import com.example.shop.repository.ShopRepository;
import org.hibernate.annotations.NotFound;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;

@Service
public class ShopAndEmployeService {

    private ShopRepository shopRepository;
    private EmployeeRepository employeeRepository;

    public ShopAndEmployeService(ShopRepository shopRepository, EmployeeRepository employeeRepository) {
        this.shopRepository = shopRepository;
        this.employeeRepository = employeeRepository;
    }

    public List<Employee> getAll(){
        List<Employee> employees = employeeRepository.findAll();
        return employees;
    }


    public Shop addRevenueAndEmployList(ShopDto shopDto,Long id ) {

        Shop myShop = new Shop(shopDto.getEmployeeList(), shopDto.getCashFlow(), shopDto.getNameOfShop());
        myShop.setEmployeeList(shopDto.getEmployeeList());
        myShop.setNameOfShop(myShop.getNameOfShop());
        myShop.setCashFlow(myShop.getCashFlow());
        myShop.setCustomers(shopDto.getCustomerSet());

        return  shopRepository.save(myShop);
    }

    public Shop putShop(ShopDto shopDto,Long id) {
        Shop shop = new Shop(shopDto.getEmployeeList(),shopDto.getCashFlow(),shopDto.getNameOfShop());
        shop.setEmployeeList(shopDto.getEmployeeList());
        shop.setCashFlow(shopDto.getCashFlow());
        shop.setNameOfShop(shopDto.getNameOfShop());
        shopRepository.save(shop);
        return shop;
    }

    public String addEmployee(EmployeeDto employeeDto, Long id) {
       Shop shop = shopRepository.findById(id).orElseThrow(()-> new EntityNotFoundException("NotFound"));
       Employee newEmployee = new Employee(employeeDto.getAge(),employeeDto.getName(),employeeDto.getShop());
       newEmployee.setAge(employeeDto.getAge());
       newEmployee.setName(employeeDto.getName());
        newEmployee.setShop(shop);
        employeeRepository.save(newEmployee);
        return " ok " ;

    }


    public Employee updateEmployee(EmployeeDto employeeDto, Long id) throws EntityNotFound {
        return employeeRepository.findById(id).map(newEmployee -> {
            newEmployee.setAge(employeeDto.getAge());
            newEmployee.setName(employeeDto.getName());
            return employeeRepository.save(newEmployee);

        }).orElseThrow(() -> new EntityNotFoundException("not Found"));
    }
}
