package com.example.shop.service;


import com.example.shop.dto.BrandDto;
import com.example.shop.exception.EntityNotFound;
import com.example.shop.model.Brand;
import com.example.shop.model.Customer;
import com.example.shop.model.Products;
import com.example.shop.repository.BrandRepository;
import com.example.shop.repository.CustomerRepository;
import com.example.shop.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;

@Service
public class BrandService {

    @Autowired
    BrandRepository brandRepository;
    ProductRepository productRepository;
    CustomerRepository customerRepository;

    public BrandService(BrandRepository brandRepository, ProductRepository productRepository) {
        this.brandRepository = brandRepository;
        this.productRepository = productRepository;
    }

    public List<Brand> getAll(List id) {
        return brandRepository.findAll();
    }

    public String addBrand(BrandDto brandDto, Long id) {
        Products products = productRepository.findAllById(id).orElseThrow(() -> new EntityNotFoundException("not found"));
        Customer customer = new Customer();
        Brand addBrand = new Brand(brandDto.getNameOfBrand(), brandDto.getPreferedBrand(), brandDto.getProducts());
        addBrand.setNameOfBrand(brandDto.getNameOfBrand());
        addBrand.setPreferedBrand(brandDto.getPreferedBrand());
        addBrand.setProducts(brandDto.getProducts());
        addBrand.setProducts(products);
        addBrand.setCustomer(customer);
        brandRepository.save(addBrand);
        return "added to brand";

    }

    public Optional<Brand> updateBrand(BrandDto brandDto, Long id) {
        return brandRepository.findAllById(id).map(updateBrands -> {
            Customer customer = new Customer();
            Products products = new Products();
            updateBrands.setPreferedBrand(brandDto.getPreferedBrand());
            updateBrands.setNameOfBrand(brandDto.getNameOfBrand());
            updateBrands.setCustomer(customer);
            updateBrands.setProducts(products);
            return brandRepository.save(updateBrands);
        });
    }

    public Brand getBrands(Long id) throws EntityNotFound {
        Optional<Brand> optionalBrand = brandRepository.findAllById(id);
        if (optionalBrand.isPresent()) {
            return optionalBrand.get();
        }
        throw new EntityNotFound("Not Found");
    }

    public String deleteByID(Long id) {
        brandRepository.deleteById(id);
        return "deleted brand  id ";
    }


//  todo-> remake this , returnes null

    public Optional<Object> customerLikesBrand(BrandDto brandDto, Long id) {
        return brandRepository.findAllById(id).map(customerLikesBrand -> {

            Customer customer = new Customer();
            customerLikesBrand.getCustomer();
            customerLikesBrand.getNameOfBrand();
            customer.getBrandList().add(customerLikesBrand);
            brandRepository.save(customerLikesBrand);
            return customerRepository.save(customer);


        });
    }

}




