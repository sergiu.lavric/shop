package com.example.shop.service;

import com.example.shop.dto.CustomerDto;

import com.example.shop.dto.ShopDto;
import com.example.shop.exception.EntityNotFound;
import com.example.shop.model.Customer;

import com.example.shop.model.Shop;
import com.example.shop.repository.CustomerRepository;
import com.example.shop.repository.ShopRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.Optional;


@Service
public class ShopAndCustomerService {

    @Autowired
    ShopRepository shopRepository;
    CustomerRepository customerRepository;

    public ShopAndCustomerService(ShopRepository shopRepository,CustomerRepository customerRepository) {
        this.shopRepository = shopRepository;
        this.customerRepository = customerRepository;
    }


    public String  addCustomer(CustomerDto customerDto,Long id) {
        Shop shop = shopRepository.findById(id).orElseThrow(()-> new EntityNotFoundException("NotFound"));
        Customer newCustomer = new Customer(customerDto.getName(), customerDto.getShoppingBag(), customerDto.getCustomerSet());
        newCustomer.setName(customerDto.getName());
        newCustomer.setShoppingBags(customerDto.getShoppingBag());
        shop.getCustomers().add(newCustomer);
        customerRepository.save(newCustomer);

        return " customer added to shop  " ;


    }
    public String addCustomerToShop(CustomerDto customerDto, ShopDto shopDto )  {
        Shop shop=new Shop(shopDto.getNameOfShop(),shopDto.getCashFlow());
        Customer alex = new Customer(customerDto.getName(),customerDto.getShoppingBag());
        Customer george = new Customer(customerDto.getName(),customerDto.getShoppingBag());
        shop.getCustomers().add(alex);
        shop.getCustomers().add(george);
        shopRepository.save(shop);
        return "ok";
    }

    public Optional<Customer> getCustomer(Long id) {
      Optional<Customer> customerList = customerRepository.findAllById(id);
        return customerList;

    }
    public Customer updateCustomer (CustomerDto customerDto, Long id) throws EntityNotFound {
        return customerRepository.findById(id).map(newCustomer -> {
            newCustomer.setName(newCustomer.getName());
            newCustomer.setShoppingBags(newCustomer.getShoppingBags());
            return customerRepository.save(newCustomer);

        }).orElseThrow(() -> new EntityNotFoundException("not Found"));
    }

}
