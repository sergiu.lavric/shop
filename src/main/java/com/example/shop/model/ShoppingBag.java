package com.example.shop.model;


import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;



@Table(name = "shoppingBag")
@Entity
public class ShoppingBag {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @JsonIgnore
    @OneToOne
    @JoinColumn(name = "shopping_bag_customer")
    private Customer customer;


    @JsonIgnore
    @OneToMany(mappedBy = "shoppingBag",cascade = CascadeType.ALL)
    private List<Products> productsList = new ArrayList<>();

    private String name;



    public ShoppingBag() {
    }

    public ShoppingBag(Customer customer, List<Products> productsList) {
        this.customer = customer;
        this.productsList = productsList;
    }

    public ShoppingBag(String name, Customer customer) {
        this.name = name;
        this.customer=customer;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public List<Products> getProductsList() {
        return productsList;
    }

    public void setProductsList(List<Products> productsList) {
        this.productsList = productsList;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    @Override
    public String toString() {
        return "ShoppingBag{" +
                "id=" + id +
                ", customer=" + customer +
                ", productsList=" + productsList +
                '}';
    }
}
