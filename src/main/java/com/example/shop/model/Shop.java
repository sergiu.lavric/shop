package com.example.shop.model;


import lombok.Getter;

import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


@Getter
@Setter

@Entity
@Table(name = "shop")
public class Shop {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToMany(mappedBy = "shop", cascade = CascadeType.ALL)
    private List<Employee> employeeList = new ArrayList<>() ;


@ManyToMany(cascade = {CascadeType.PERSIST,CascadeType.MERGE})
@JoinTable(name = "shop_customer",joinColumns = @JoinColumn(name="shop"),inverseJoinColumns = @JoinColumn(name = "customers"))
     private Set <Customer> customers = new HashSet<>();

    private Integer cashFlow;

    private String nameOfShop;

    public Shop() {
    }

    public Shop(String nameOfShop, Integer cashFlow) {
        this.nameOfShop = nameOfShop;
        this.cashFlow = cashFlow;
    }

    public Shop( List<Employee> employeeList,  Integer cashFlow, String nameOfShop) {

        this.employeeList = employeeList;
        this.cashFlow = cashFlow;
        this.nameOfShop = nameOfShop;
    }

    public Shop(Long id, List<Employee> employeeList, Set<Customer> customers, Integer cashFlow, String nameOfShop) {
        this.id = id;
        this.employeeList = employeeList;
        this.customers = customers;
        this.cashFlow = cashFlow;
        this.nameOfShop = nameOfShop;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<Employee> getEmployeeList() {
        return employeeList;
    }

    public void setEmployeeList(List<Employee> employeeList) {
        this.employeeList = employeeList;
    }

    public Set<Customer> getCustomers() {
        return customers;
    }

    public void setCustomers(Set<Customer> customers) {
        this.customers = customers;
    }

    public Integer getCashFlow() {
        return cashFlow;
    }

    public void setCashFlow(Integer cashFlow) {
        this.cashFlow = cashFlow;
    }

    public String getNameOfShop() {
        return nameOfShop;
    }

    public void setNameOfShop(String nameOfShop) {
        this.nameOfShop = nameOfShop;
    }


}
