package com.example.shop.model;


import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity

public class Brand {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    String nameOfBrand;
    private Boolean preferedBrand;

    @ManyToOne(fetch =FetchType.LAZY,cascade = CascadeType.ALL)
    @JoinColumn(name = "customers_id")
    private Customer customer;
    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    @JoinColumn(name = "products_id")
    private Products products;

    public Brand() {
    }

    public Brand(String nameOfBrand, Boolean preferedBrand, Products products) {
        this.nameOfBrand = nameOfBrand;
        this.preferedBrand=preferedBrand;
        this.products=products;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }
    public String getNameOfBrand() {
        return nameOfBrand;
    }

    public void setNameOfBrand(String nameOfBrand) {
        this.nameOfBrand = nameOfBrand;
    }

    public Boolean getPreferedBrand() {
        return preferedBrand;
    }

    public void setPreferedBrand(Boolean preferedBrand) {
        this.preferedBrand = preferedBrand;
    }

    public Products getProducts() {
        return products;
    }

    public void setProducts(Products products) {

        this.products = products;
    }
}

