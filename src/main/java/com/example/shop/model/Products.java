package com.example.shop.model;


import javax.persistence.*;

import java.util.ArrayList;
import java.util.HashSet;

import java.util.List;
import java.util.Set;


@Entity
@Table(name = "products")
public class Products {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String typeOfProduct;
    private Integer amountInBag;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "shoping_bag_id")
    private ShoppingBag shoppingBag;

    @OneToMany(mappedBy = "products" ,cascade = CascadeType.ALL)
    private List<Brand> brandList =new ArrayList<>();


//    @ManyToMany(cascade = {CascadeType.PERSIST,CascadeType.MERGE})
//    @JoinTable(name = "shop_customer",joinColumns = @JoinColumn(name="shop"),inverseJoinColumns = @JoinColumn(name = "customers"))
//    private Set <Customer> customers = new HashSet<>();

    public Products() {
    }


    public List<Brand> getBrandList() {
        return brandList;
    }

    public void setBrandList(List<Brand> brandList) {
        this.brandList = brandList;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTypeOfProduct() {
        return typeOfProduct;
    }

    public void setTypeOfProduct(String typeOfProduct) {
        this.typeOfProduct = typeOfProduct;
    }

    public Integer getAmountInBag() {
        return amountInBag;
    }

    public void setAmountInBag(Integer amountInBag) {
        this.amountInBag = amountInBag;
    }

    public ShoppingBag getShoppingBag(Products products) {
        return shoppingBag;
    }

    public void setShoppingBag(ShoppingBag shoppingBag) {
        this.shoppingBag = shoppingBag;
    }
}
