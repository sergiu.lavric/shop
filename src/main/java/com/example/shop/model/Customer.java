package com.example.shop.model;


import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.NaturalId;

import javax.persistence.*;
import java.util.*;


@Entity

public class Customer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NaturalId
    String name;

    @JsonIgnore
    @OneToOne(mappedBy = "customer" ,cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    private ShoppingBag shoppingBags;

    @ManyToMany(mappedBy = "customers")
    private Set<Shop> shopSet = new HashSet<>();


    @OneToMany(mappedBy = "customer" ,cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    private List<Brand> brandList;




    public Customer(String name) {
        this.name = name;
    }



    public Customer(String name, ShoppingBag shoppingBag, Set<Shop> shopSet) {
        this.name = name;
        this.shoppingBags = shoppingBag;
        this.shopSet = shopSet;
    }

    public Customer(ShoppingBag shoppingBag, Set<Shop> shopSet) {
        this.shoppingBags =shoppingBag;
        this.shopSet=shopSet;
    }

    public List<Brand> getBrandList() {
        return brandList;
    }

    public void setBrandList(List<Brand> brandList) {
        this.brandList = brandList;
    }

    public Customer() {

    }

    public Customer(ShoppingBag shoppingBags) {
        this.shoppingBags = shoppingBags;
    }

    public Customer(String name, ShoppingBag shoppingBag) {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public Long setId(Long id) {
        return this.id = id;

    }

    public ShoppingBag getShoppingBags() {
        return shoppingBags;
    }

    public void setShoppingBags(ShoppingBag shoppingBags) {
        this.shoppingBags = shoppingBags;
    }

    public Set<Shop> getShopSet() {
        return shopSet;
    }

    public void setShopSet(Set<Shop> shopSet) {
        this.shopSet = shopSet;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Customer customer = (Customer) o;
        return Objects.equals(name,customer.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash( name);
    }
}

