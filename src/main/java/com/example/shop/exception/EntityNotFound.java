package com.example.shop.exception;

import javax.swing.text.html.parser.Entity;

public class EntityNotFound  extends Exception {

    public EntityNotFound (String msg) {
        super(msg);

    }

}
