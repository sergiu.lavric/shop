package com.example.shop.dto;


import com.example.shop.model.Shop;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


public class    EmployeeDto {
    private Long id;

    private Integer age;

    private String name;

    private Shop shop;

    public EmployeeDto() {
    }

    public EmployeeDto(Integer age, String name, Shop shop) {
        this.age = age;
        this.name = name;
        this.shop = shop;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Shop getShop() {
        return shop;
    }

    public void setShop(Shop shop) {
        this.shop = shop;
    }
}
