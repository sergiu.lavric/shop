package com.example.shop.dto;


import com.example.shop.model.Brand;
import com.example.shop.model.Customer;
import com.example.shop.model.Shop;
import com.example.shop.model.ShoppingBag;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;
import java.util.Set;

@NoArgsConstructor
@Getter
@Setter
public class CustomerDto {
    private Long id;
    private String name;
    private Set<Shop> customerSet;
    private ShoppingBag shoppingBag;
    private Customer customer;
    private List<Brand> brand;

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public List<Brand> getBrand() {
        return brand;
    }

    public void setBrand(List<Brand> brand) {
        this.brand = brand;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Shop> getCustomerSet() {
        return customerSet;
    }

    public void setCustomerSet(Set<Shop> customerSet) {
        this.customerSet = customerSet;
    }

    public ShoppingBag getShoppingBag() {
        return shoppingBag;
    }

    public void setShoppingBag(ShoppingBag shoppingBag) {
        this.shoppingBag = shoppingBag;
    }
}
