package com.example.shop.dto;

import com.example.shop.model.Customer;
import com.example.shop.model.Products;
import com.example.shop.model.ShoppingBag;

import java.util.List;

public class ShoppingBagDto {

    private Long id;
    private String name;
    private Customer customer;
    private List<Products> productsList;




    public ShoppingBagDto() {
    }

    public ShoppingBagDto(Customer customer, List<Products> productsList) {
        this.customer = customer;
        this.productsList = productsList;
    }

    public ShoppingBagDto(String name, Customer customer) {
        this.name = name;
        this.customer = customer;
    }

    public String getName() {return name; }

    public void setName(String name) {  this.name = name; }
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Customer getCustomer() {
        return customer;
    }

    public Customer setCustomer(Customer customer) {return this.customer = customer; }

    public List<Products> getProductsList() {
        return productsList;
    }


    public void setProductsList(Integer amountInBag, String typeOfProduct, ShoppingBag shoppingBag) {
    }
}
