package com.example.shop.dto;

import com.example.shop.model.Customer;
import com.example.shop.model.Products;

public class BrandDto {
    private Long id;
    String nameOfBrand;
    Boolean preferedBrand;
    private Products products;
    private Customer customer;

    public BrandDto() {
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNameOfBrand() {
        return nameOfBrand;
    }

    public void setNameOfBrand(String nameOfBrand) {
        this.nameOfBrand = nameOfBrand;
    }

    public Boolean getPreferedBrand() {
        return preferedBrand;
    }

    public void setPreferedBrand(Boolean preferedBrand) {
        this.preferedBrand = preferedBrand;
    }

    public Products getProducts() {
        return products;
    }

    public void setProducts(Products products) {
        this.products = products;
    }
}
