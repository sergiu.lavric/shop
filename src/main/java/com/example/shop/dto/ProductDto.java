package com.example.shop.dto;

import com.example.shop.model.ShoppingBag;

public class ProductDto {

    private Long id;
    private String typeOfProduct;
    private Integer amountInBag;
    private ShoppingBag shoppingBag;

    public ProductDto() {
    }

    public ProductDto(String typeOfProduct, Integer amountInBag, ShoppingBag shoppingBag) {
        this.typeOfProduct = typeOfProduct;
        this.amountInBag = amountInBag;
        this.shoppingBag = shoppingBag;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTypeOfProduct() {
        return typeOfProduct;
    }

    public void setTypeOfProduct(String typeOfProduct) {
        this.typeOfProduct = typeOfProduct;
    }

    public Integer getAmountInBag() {
        return amountInBag;
    }

    public void setAmountInBag(Integer amountInBag) {
        this.amountInBag = amountInBag;
    }

    public ShoppingBag getShoppingBag() {
        return shoppingBag;
    }

    public void setShoppingBag(ShoppingBag shoppingBag) {
        this.shoppingBag = shoppingBag;
    }

    @Override
    public String toString() {
        return "ProductDto{" +
                "id=" + id +
                ", typeOfProduct='" + typeOfProduct + '\'' +
                ", amountInBag=" + amountInBag +
                ", shoppingBag=" + shoppingBag +
                '}';
    }
}
