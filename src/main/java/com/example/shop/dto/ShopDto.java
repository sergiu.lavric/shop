package com.example.shop.dto;

import com.example.shop.model.Customer;
import com.example.shop.model.Employee;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;
import java.util.Set;


@Getter
@Setter
public class ShopDto {

    private Long id;
    private Integer cashFlow;
    private String nameOfShop;
    private List<Employee> employeeList;

    private Set<Customer> customerSet ;

    public ShopDto() {
    }

    public ShopDto(Integer cashFlow, String nameOfShop, List<Employee> employeeList, Set<Customer> customerSet) {
        this.cashFlow = cashFlow;
        this.nameOfShop = nameOfShop;
        this.employeeList = employeeList;
        this.customerSet = customerSet;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getCashFlow() {
        return cashFlow;
    }

    public void setCashFlow(Integer cashFlow) {
        this.cashFlow = cashFlow;
    }

    public String getNameOfShop() {
        return nameOfShop;
    }

    public void setNameOfShop(String nameOfShop) {
        this.nameOfShop = nameOfShop;
    }

    public List<Employee> getEmployeeList() {
        return employeeList;
    }

    public void setEmployeeList(List<Employee> employeeList) {
        this.employeeList = employeeList;
    }
}
