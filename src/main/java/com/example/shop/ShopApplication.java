package com.example.shop;

import com.example.shop.model.Customer;
import com.example.shop.model.Shop;
import com.example.shop.repository.CustomerRepository;
import com.example.shop.repository.ShopRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ShopApplication  implements CommandLineRunner {

    @Autowired
    ShopRepository shopRepository;
    CustomerRepository customerRepository;

    public static void main(String[] args) {
        SpringApplication.run(ShopApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
//        generateShop();
//        generateCusotmer();

    }
     public  void generateShop(){
         Shop Emag = new Shop("Emag",210000 );
         Shop MyStore = new Shop("My Store",18000);
         Shop Amazon = new Shop("Amazon",1234556789);


         shopRepository.save(Emag);
         shopRepository.save(MyStore);
         shopRepository.save(Amazon);
     }
     public void generateCusotmer(){
         Customer Alex = new Customer();
         Customer Geroge = new Customer();

         Shop Emag = new Shop("Emag",210000 );
         Shop MyStore = new Shop("My Store",18000);
         Shop Amazon = new Shop("Amazon",1234556789);

         Alex.getShopSet().add(Emag);
         Geroge.getShopSet().add(Amazon);

         shopRepository.save(Emag);
         shopRepository.save(Amazon);

     }
}
